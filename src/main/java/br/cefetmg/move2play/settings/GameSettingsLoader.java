package br.cefetmg.move2play.settings;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.net.URLDecoder;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Allows saving and loading game settings.
 *
 * @author fegemo
 */
public class GameSettingsLoader {

    private final Class ownerClass;
    private final Gson gson;

    private static final String SETTINGS_FILE_NAME = "game-settings.json";
    private static final Logger LOGGER = Logger.getLogger(GameSettingsLoader.class.getName());

    public GameSettingsLoader(Class ownerClass) {
        this.ownerClass = ownerClass;
        this.gson = new Gson();
    }

    /**
     * Gets It uses the owner class to locate the game settings file.
     *
     * @return the string containing the filepath
     */
    public String getSettingsFilePath() {
        String settingsPath = "";
        try {
            String classPath = ownerClass.getProtectionDomain().getCodeSource().getLocation().getPath();
            String decodedPath = URLDecoder.decode(classPath, "UTF-8");
            File execPath = new File(decodedPath);
            settingsPath = execPath.getParent() + File.separator + SETTINGS_FILE_NAME;
            File f = new File(settingsPath);
            if (f.exists() && !f.isDirectory()) {
                return settingsPath;
            } else {
                System.out.println("Não existe " + settingsPath);
                File f2 = new File(SETTINGS_FILE_NAME);
                if (f2.exists() && !f2.isDirectory()) {
                    return SETTINGS_FILE_NAME;
                } else {
                    throw new Exception();
                }
            }
        } catch (Exception e) {
            System.out.println("Exceção exec...  ao achar " + settingsPath);
            try {
                File callPath = new File(".");
                String execPathStr = callPath.getAbsolutePath().substring(0, callPath.getAbsolutePath().length() - 1);
                settingsPath = execPathStr + SETTINGS_FILE_NAME;
                File f = new File(settingsPath);
                if (f.exists() && !f.isDirectory()) {
                    return settingsPath;
                } else {
                    System.out.println("Não existe " + settingsPath);
                    return SETTINGS_FILE_NAME;
                }
            } catch (Exception ex) {
                System.out.println("Exceção call...  ao achar " + settingsPath);
                return SETTINGS_FILE_NAME;
            }
        }
    }

    @SuppressWarnings("unchecked")
    public <T extends GameSettings> T loadSettings() {
        return (T) loadSettings(GameSettings.class);
    }

    public <T extends GameSettings> T loadSettings(Class<T> settingsClass) {
        return loadSettings(getSettingsFilePath(), settingsClass);
    }

    private <T extends GameSettings> T loadSettings(String path, Class<T> settingsClass) {
        try (Reader reader = new FileReader(path)) {
            return gson.fromJson(reader, settingsClass);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Could not load game settings at " + path, ex);
        }

        return null;
    }

    public void saveSettings() {
        saveSettings(getSettingsFilePath());
    }

    private void saveSettings(String path) {
        try (Writer writer = new FileWriter(path)) {
            gson.toJson(this, writer);
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, "Could not save game settings at " + path, ex);
        }
    }
}
