package br.cefetmg.move2play.settings;

import java.util.List;

/**
 * Game configuration used by the console and available to the game.
 *
 * @author Thiago Figueiredo Costa
 */
public class GameSettings {

    // basic game configuration
    private String name;
    private String UUID;
    private String description;
    private String instructions;
    private int minPlayers;
    private int maxPlayers;
    private boolean allowNewPlayersDuringMatch;

    // marketing related information
    private String version;
    private List<String> categories;
    private String developerName;
    private String developerContact;
    private String storeId;
    private List<String> images;
    private int coverImageIndex;

    // game loading information for the console (eg, game-opener)
    private GameLoaderData gameLoader;

    // configurable settings
    private List<Integer> matchDurations;
    private int matchDurationIndex;
    private CustomSetting[] gameSpecificSettings;

    /**
     *
     * Get a specific setting of the game by name.
     *
     * @param name The name of the attribute.
     * @return The CustomSetting found or null.
     * @see br.cefetmg.move2play.settings.CustomSetting
     */
    public CustomSetting getSpecificSetting(String name) {
        for (CustomSetting gso : gameSpecificSettings) {
            if (gso.getName().equals(name)) {
                return gso;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUUID() {
        return UUID;
    }

    public void setUUID(String UUID) {
        this.UUID = UUID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public boolean isAllowNewPlayersDuringMatch() {
        return allowNewPlayersDuringMatch;
    }

    public void setAllowNewPlayersDuringMatch(boolean allowNewPlayersDuringMatch) {
        this.allowNewPlayersDuringMatch = allowNewPlayersDuringMatch;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public List<String> getCategories() {
        return categories;
    }

    public void setCategories(List<String> categories) {
        this.categories = categories;
    }

    public String getDeveloperName() {
        return developerName;
    }

    public void setDeveloperName(String developerName) {
        this.developerName = developerName;
    }

    public String getDeveloperContact() {
        return developerContact;
    }

    public void setDeveloperContact(String developerContact) {
        this.developerContact = developerContact;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public int getCoverImageIndex() {
        return coverImageIndex;
    }

    public void setCoverImageIndex(int coverImageIndex) {
        this.coverImageIndex = coverImageIndex;
    }

    public List<String> getImages() {
        return images;
    }

    public void setScreenshots(List<String> images) {
        this.images = images;
    }

    public List<Integer> getMatchDurations() {
        return matchDurations;
    }

    public void setMatchDurations(List<Integer> matchDurations) {
        this.matchDurations = matchDurations;
    }

    public int getMatchDurationIndex() {
        return matchDurationIndex;
    }

    public void setMatchDurationIndex(int matchDurationIndex) {
        this.matchDurationIndex = matchDurationIndex;
    }

    public CustomSetting[] getGameSpecificSettings() {
        return gameSpecificSettings;
    }

    public void setGameSpecificSettings(CustomSetting[] gameSpecificSettings) {
        this.gameSpecificSettings = gameSpecificSettings;
    }

    public GameLoaderData getGameLoader() {
        return gameLoader;
    }

    public void setGameLoader(GameLoaderData gameLoader) {
        this.gameLoader = gameLoader;
    }
}
