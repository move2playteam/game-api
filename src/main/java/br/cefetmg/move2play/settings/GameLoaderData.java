package br.cefetmg.move2play.settings;

/**
 * Describes a form of loading a game (such as the game-opener way).
 *
 * @author fegemo
 */
public class GameLoaderData {

    private String type;
    private CustomSetting[] settings;

    public CustomSetting[] getSettings() {
        return settings;
    }

    public void setSettings(CustomSetting[] settings) {
        this.settings = settings;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
