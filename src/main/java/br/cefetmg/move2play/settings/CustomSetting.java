package br.cefetmg.move2play.settings;

import br.cefetmg.move2play.exception.IllegalArgumentException;

/**
 * Custom game attribute specific for a game in its GameSettings.
 *
 * @author Thiago Figueiredo Costa
 */
public class CustomSetting {

    private String name;
    private String type;
    private String value;
    private boolean readOnly;
    private String description;

    public CustomSetting(String attribute, String type, String value, String description) throws IllegalArgumentException {
        this(attribute, type, value, description, false);
    }

    public CustomSetting(String name, String type, String value, String description, boolean readOnly) throws IllegalArgumentException {
        if (!knownType(type)) {
            throw new IllegalArgumentException("Invalid type(" + type + ") for setting variable");
        }
        this.name = name;
        this.type = type;
        this.value = value;
        this.readOnly = readOnly;
        this.description = description;
    }

    public static boolean knownType(String type) {
        String lowerType = type.toLowerCase();
        switch (lowerType) {
            case "int":
            case "integer":
            case "int32":
            case "char":
            case "character":
            case "byte":
            case "bool":
            case "boolean":
            case "bit":
            case "char*":
            case "string":
            case "text":
            case "double":
            case "float":
            case "long":
            case "longlong":
                return true;
            default:
                return false;
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T parse() {
        String lowerType = this.type.toLowerCase();
        switch (lowerType) {
            case "int":
            case "integer":
            case "int32":
                return (T) Integer.class.cast(Integer.parseInt(this.value));
            case "char":
            case "character":
            case "byte":
                return (T) Character.class.cast(this.value.charAt(0));
            case "bool":
            case "boolean":
            case "bit":
                if (this.value.length() > 1) {
                    if (this.value.toLowerCase().equals("false")) {
                        return (T) Boolean.class.cast(false);
                    } else {
                        return (T) Boolean.class.cast(true);
                    }
                } else {
                    return (T) Boolean.class.cast(this.value.charAt(0) > 0);
                }
            case "char*":
            case "string":
            case "text":
                return (T) String.class.cast(this.value);
            case "double":
            case "float":
                return (T) Double.class.cast(Double.parseDouble(this.value));
            case "long":
            case "longlong":
                return (T) Long.class.cast(Long.parseLong(this.value));
            default:
                return null;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) throws IllegalArgumentException {
        if (!knownType(type)) {
            throw new IllegalArgumentException("Invalid type(" + type + ") for setting variable");
        }
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    public void setReadOnly(boolean readOnly) {
        this.readOnly = readOnly;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "GameSettingObject{" + "name=" + name + ", type=" + type + ", value=" + value + ", readOnly=" + readOnly + ", description=" + description + '}';
    }
}
