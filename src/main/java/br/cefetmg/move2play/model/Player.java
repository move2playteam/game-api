package br.cefetmg.move2play.model;

/**
 * Player Class Model for Move2Play
 *
 * @author Thiago Figueiredo Costa and Luiz
 */
public class Player {

    private String uuid;
    private String name;
    private int[] color;

    public Player(String uuid, String name, int[] color) {
        this.uuid = uuid;
        this.name = name;
        this.color = color;
    }

    public Player(String uuid, String name) {
        this(uuid, name, new int[]{0, 0, 0});
    }

    public Player() {
        color = new int[]{0, 0, 0};
    }

    /**
     *
     * Gets the Universal Unique ID of the Player
     *
     * @return message The player uuid
    *
     */
    public String getUUID() {
        return uuid;
    }

    /**
     *
     * Sets the Universal Unique ID of the Player
     *
     * @param uuid The player uuid
    *
     */
    public void setUUID(String uuid) {
        this.uuid = uuid;
    }

    /**
     *
     * Gets the Name of the Player
     *
     * @return message The player uuid
    *
     */
    public String getName() {
        return name;
    }

    /**
     *
     * Sets the Name of the Player
     *
     * @param name The player name
    *
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * Gets the favorite color of the Player
     *
     * @return The player favorite color
    *
     */
    public int[] getColor() {
        return color;
    }

    /**
     *
     * Sets the favorite color of the Player
     *
     * @param color The player favorite color
    *
     */
    public void setColor(int[] color) {
        this.color = color;
    }
}
