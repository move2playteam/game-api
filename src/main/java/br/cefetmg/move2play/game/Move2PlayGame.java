package br.cefetmg.move2play.game;

import br.cefetmg.move2play.model.Player;

/**
 * A Move2PlayGame is the interface between the console and the game. Its
 * methods are called by the console.
 *
 * @author Luiz
 */
public interface Move2PlayGame {

    /**
     * Called when the console wants the game to start (boot up).
     */
    public void initGame();

    /**
     * Called when the console wants the game to close itself.
     */
    public void closeGame();

    /**
     * Called when the console wants the game to leave the waiting room and
     * start the match.
     */
    public void startMatch();

    /**
     * Called when the console wants the game to finish the match and show the
     * results.
     */
    public void finishMatch();

    /**
     * Called when the console wants the game to add a new player. The console
     * will typically call this only while the game is in the waiting room.
     * However, if the game allows players joining while it is running, the
     * console might call this during the match.
     *
     * @param player The {@code player} being added.
     * @see br.cefetmg.move2play.model.Player
     */
    public void addPlayer(Player player);

    /**
     * Called when the console wants the game to remove a player. This can be
     * called when the game is either in the waiting room or during a match.
     *
     * @param player The {@code player} being removed.
     * @see br.cefetmg.move2play.model.Player
     */
    public void removePlayer(Player player);

    /**
     * Called when the console wants the game to add a movement for the player
     * specified by the provided uuid.
     *
     * @param uuid The UUID of the player who moved.
     * @param amount The amount of movements.
     */
    public void move(String uuid, int amount);

    /**
     * Called by the console to get the current state of the game.
     *
     * @return The current game state.
     * @see br.cefetmg.move2play.game.GameState
     */
    public GameState getState();
}
