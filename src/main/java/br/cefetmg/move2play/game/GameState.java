package br.cefetmg.move2play.game;

/**
 * The state in which the game is at.
 *
 * @author fegemo
 */
public enum GameState {
    WAITING_ROOM, MATCH_RUNNING, LOADING, MATCH_RESULTS;
}
