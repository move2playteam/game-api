package br.cefetmg.move2play.exception;

/**
 * Illegal Argument Exception Class for GameSettingAttribute
 *
 * @author Thiago Figueiredo Costa
 */
public class IllegalArgumentException extends Move2PlayException{
    
    public IllegalArgumentException(String message) {
        super(message);
    }
    
}
