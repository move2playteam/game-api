package br.cefetmg.move2play.exception;

/**
 * Generic Exception Class for Move2Play
 *
 * @author Luiz
 */
public class Move2PlayException extends Exception {
    public Move2PlayException(String message){
        super(message);
    }
}
