package br.cefetmg.move2play.exception;

/**
 * Illegal Attribute Exception Class for GameSettingAttribute
 *
 * @author Thiago Figueiredo Costa
 */
public class IllegalAttributeException extends Move2PlayException{
    
    public IllegalAttributeException(String message) {
        super(message);
    }
    
}
