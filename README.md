# Game API

Esta é a **game-api** da plataforma Move2Play. Ela possui
as interfaces e classes necessárias à comunicação entre
console e jogo. Sendo assim, ela é um artefato de software
consumido por:

1. _console_
1. _game-opener_
1. jogos

A mais importante interface da _game-api_ é a `Move2PlayGame`,
que deve ser implementada pela classe principal de cada jogo.
Ela contém os métodos que são invocados pelo _console_ (por 
meio do _game-opener_) oportunamente (por exemplo, quando
um jogador é adicionado, quando a partida deve começar etc.).
Essa interface está descrita na imagem a seguir.

![](docs/move2playgame.png)


## Instalação

A _game api_ pode ser compilada tanto com JDK8 (para os jogos e _game-opener_) quanto com JDK11 (para o _console_).

Ela é um projeto Gradle 🐘, portanto, as IDEs conseguem compilar  normalmente. A tarefa usada para gerar um `.jar`
se chama `jar` (ie, `gradle jar`). Contudo, em vez dos
outros projetos incluírem a dependência da `game-api.jar`,
eles estão configurados para incluir o projeto da _game-api_
como um todo (sendo, portanto, desnecessário gerar um `.jar`) dela.


## Execução

A _game-api_ não é uma aplicação. Logo, não é possível executá-la.